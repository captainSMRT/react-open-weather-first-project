import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HeaderComponent from './HeaderComponent.jsx'
import FooterComponent from './FooterComponent.jsx'
import ContentComponent from './ContentComponent.jsx'

class WeatherApp extends Component {
  render() {
    return (
      <div className="WeatherApp">
        <Router>
          <>
            <HeaderComponent/>
            <Switch>
              <Route path="/" exact component={ContentComponent} />
            </Switch>
            <FooterComponent/>
          </>
        </Router>
      </div>
    );
  }
}


export default WeatherApp;
