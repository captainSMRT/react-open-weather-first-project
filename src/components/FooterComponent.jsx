import React, { Component } from "react";

class FooterComponent extends Component {
    render(){
      return (
        <footer className="footer fixed-bottom">
           <span className="text-muted"> All right reserved by Kai Xuan</span>
        </footer>
      )
    }
  }
  export default FooterComponent