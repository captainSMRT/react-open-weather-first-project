import React, { Component } from "react";
import GetAllWeatherService from '../api/GetAllWeatherService.js'
import GetAllCountryCodeService from '../api/GetAllCountryCodeService.js'
import ReactDOM from 'react-dom'

class ContentComponent extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            isInformationFound: true, 
            isCountryFound: true,
            isEmpty: true,
            id: 0,
            country: "",
            city: "",
            weathers: [],
            countries: "",
        }
        this.handleChange = this.handleChange.bind(this)
        this.addButtonClicked = this.addButtonClicked.bind(this)
        this.renderData = this.renderData.bind(this)
        this.deleteTileButtonClicked = this.deleteTileButtonClicked.bind(this)
        this.resetAllButtonClicked = this.resetAllButtonClicked.bind(this)
        this.refreshTileByInterval = this.refreshTileByInterval.bind(this)
        this.onChangeRefreshInterval = this.onChangeRefreshInterval.bind(this)
        this.toggleSettings = this.toggleSettings.bind(this)
        this.preventPropagation = this.preventPropagation.bind(this)
    }

    componentDidMount() {
        const countryResponse = GetAllCountryCodeService.executeGetAllCountryCodeService()
        this.setState({
            countries: countryResponse
        })

 
    }

    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }
    
    refreshTileByInterval() {
        let currentTime = new Date();
        var result = this.state.weathers
        for(let i = 0; i < this.state.weathers.length; i++) {
            if ((Number(this.state.weathers[i].lastRefresh) + this.state.weathers[i].refreshInterval*60000) < Number(currentTime)){
                GetAllWeatherService.executeGetWeatherByID(this.state.weathers[i].id)
            .then(response => {
                   
                let currentTime = new Date()
                response.data.idx = result[i].idx
                response.data.refreshInterval = result[i].refreshInterval
                response.data.lastRefresh = currentTime
                result[i] = response.data
                this.setState({weathers:result})  
            })
            .catch()
            }
        }
    }

    addButtonClicked() {
        setInterval(() => this.refreshTileByInterval(),1000)
        this.setState({id:this.state.id+1})
        const countryToFind = this.state.countries.get(this.state.country.toLowerCase())
        if(countryToFind != undefined) {
            GetAllWeatherService.executeGetAllWeatherService(this.state.city, countryToFind)
            .then(response => {
                if(response.data.sys.country === countryToFind) {                    
                    let currentTime = new Date();
                    response.data.idx = "tile"+this.state.id
                    response.data.refreshInterval = 15;
                    response.data.lastRefresh = currentTime;
                    var result = this.state.weathers
                    result.push(response.data)
                    this.setState({weathers:result, isEmpty: false, isCountryFound: true, isInformationFound: true})
                } else {
                    this.setState({isInformationFound: false})
                }
            })
            .catch(
                this.setState({isInformationFound: false})
            )
            this.render()
        } else {
            this.setState({isCountryFound: false})
        }
    }

    deleteTileButtonClicked(weather) {
        const newWeathers = this.state.weathers.filter(i => i.idx !== weather.idx)
        this.setState({weathers: newWeathers})

        if(this.state.weathers.length<=1) {
            this.setState({isEmpty: true})
        }
    }

    resetAllButtonClicked() {
        if (window.confirm('Are you sure you want to reset?')) {
            this.setState({weathers: [], isEmpty: true})
        } 
    }

    onChangeRefreshInterval(weatherId, event) {
        var list = this.state.weathers
        for(var i = 0; i < list.length; i++) {
            if(list[i].idx === weatherId) {
                list[i].refreshInterval = event.target.value
                break
            }
        }
        this.setState({
            weathers: list
        });
        console.log(this.state.weathers)
    }

    toggleSettings(weatherId, e) {
        var settings = document.getElementById(weatherId).children[0].children[document.getElementById(weatherId).children[0].children.length-1].classList
        settings.toggle("invisible")
        //node.classList.toggle('btn-menu-open');
    }

    preventPropagation(event) {
        event.stopPropagation();
    }
    renderData() {
        return  this.state.weathers.map (
            weather => {
                let weatherId = weather.idx
                let time = weather.lastRefresh.getHours() + ":" + weather.lastRefresh.getMinutes() + ":" + weather.lastRefresh.getSeconds()
                let nextTime = new Date(Number(weather.lastRefresh) + weather.refreshInterval*60000)
                nextTime = nextTime.getHours()  + ":" + nextTime.getMinutes() + ":" + nextTime.getSeconds()
                let imagePath = "/images/weather-"
                switch(weather.weather[0].main){
                    case "Thunderstorm":
                        imagePath = imagePath + "thunderstorm.png"
                        break
                    case "Drizzle":
                        imagePath = imagePath + "drizzle.png"
                        break
                    case "Rain":
                        imagePath = imagePath + "rain.png"
                        break
                    case "Snow":
                        imagePath = imagePath + "snow.png"
                        break
                    case "Clear":
                        imagePath = imagePath + "clear.png"
                        break
                    case "Clouds":
                        imagePath = imagePath + "clouds.png"
                        break
                    default:
                        imagePath = imagePath + "atmosphere.png"

                }
            return (
                <div className="col-xs-11 col-sm-11 col-md-5 col-lg-3 col-xl-3 tile" key={weather.idx} id={weather.idx} onClick={this.toggleSettings.bind(this,weatherId)}>
                        <div className="inner-tile">
                        <div className="row">
                            <div className="col-md-12 d-flex">
                            <div className="text-left">
                                <h3>{weather.name}</h3>
                            </div>
                            <div className="ml-auto">
                                <button type="button" className={`close ${weather.idx}`} aria-label="Close" onClick={this.deleteTileButtonClicked.bind(this, weather)}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <img src={imagePath}  alt={weather.weather[0]} className="img-responsive, img-tile"/>
                        </div>
                        <div className="row">
                            <div className="col-md-12 d-flex">
                            <div className="text-left">
                                <h4>{(weather.main.temp - 273.15).toFixed(1).toString() + "\u2103"}</h4>
                            </div>
                            <div className="ml-auto">
                                <h4>{weather.weather[0].description }</h4>
                            </div>
                            </div>
                        </div>
                        <div className="row settings invisible" onClick={this.preventPropagation}>
                            <div className="col-md-12">
                            <div className="text-left">
                                <h6>Refresh Interval (min): <input type="number" className="w-25 text-center" name="city" value={(weather.refreshInterval)} onChange={this.onChangeRefreshInterval.bind(this, weatherId)}/></h6>
                            </div>
                            <div className="text-left">
                                <h6>Last Refresh: {time}
                                </h6>
                            </div>
                            <div className="text-left">
                                <h6>Next Refresh: {nextTime}
                                </h6>
                            </div>
                            </div>
                            </div>
                        </div>
                        
                </div>
            )
            }
        )
    }
    render() {
        return (
            <div>
                <h1>Weather Forecast</h1>

                <div className="container-fluid">
                  <div className="row weather-dashboard" id="weather-dashboard">
                          {this.renderData()}
                  </div>
                </div>
    
                <div className="container settings-container">
                    <div className="row">
                        <div className="input-header">
                            <h4>Country:</h4>
                        </div>
                    </div>
                    <div className="row">
                            <input className="input-control" type="text" name="country" value={this.state.country} onChange={this.handleChange} placeholder="e.g. Armenia"/>
                            {!this.state.isCountryFound && <span className="text-danger font-weight-bold">Country not found.</span>}
                    </div>
                    <div className="row">
                        <div className="input-header">
                            <h4>City:</h4>
                        </div>
                    </div>
                    <div className="row">
                            <input className="input-control" type="text" name="city" value={this.state.city} onChange={this.handleChange} placeholder="e.g. Yerevan"/>
                            {!this.state.isInformationFound && <span className="text-danger font-weight-bold">City could not be found.</span>}
                    </div>
                    <div className="row">
                    <button className="btn btn-success mt-3 input-control" onClick={this.addButtonClicked}>Add</button>
                    </div>

                    <div className="row">
                    {!this.state.isEmpty && <button className="btn btn-danger mt-3 input-control" onClick={this.resetAllButtonClicked}>Reset</button>}
                    </div>
                </div>
            </div>
        )
    }
}

export default ContentComponent