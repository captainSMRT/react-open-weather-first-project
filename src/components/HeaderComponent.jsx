import React, { Component } from "react";
import { BrowserRouter as Router, withRouter, Route, Switch, Link } from "react-router-dom";

class HeaderComponent extends Component {
    render(){

      
      return (
        <header>
          <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <div><a href="https://www.linkedin.com/in/ong-kai-xuan-561a75b9/" className="navbar-brand">Info</a></div>
            <ul className="navbar-nav">
              {<li><Link className="nav-link" to="/">Home</Link></li>}
            </ul>
          </nav>
        </header>
      )
    }
  }

  export default HeaderComponent