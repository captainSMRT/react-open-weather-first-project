import axios from "axios";

class GetAllCountryCodeService {
  executeGetAllCountryCodeService() {
    //return axios.get('https://api.printful.com/countries')

    const countries = [
      "afghanistan",
      "albania",
      "algeria",
      "american samoa",
      "andorra",
      "angola",
      "anguilla",
      "antarctica",
      "antigua and barbuda",
      "argentina",
      "armenia",
      "aruba",
      "australia",
      "austria",
      "azerbaijan",
      "bahamas (the)",
      "bahrain",
      "bangladesh",
      "barbados",
      "belarus",
      "belgium",
      "belize",
      "benin",
      "bermuda",
      "bhutan",
      "bolivia (plurinational state of)",
      "bonaire, sint eustatius and saba",
      "bosnia and herzegovina",
      "botswana",
      "bouvet island",
      "brazil",
      "british indian ocean territory (the)",
      "brunei darussalam",
      "bulgaria",
      "burkina faso",
      "burundi",
      "cabo verde",
      "cambodia",
      "cameroon",
      "canada",
      "cayman islands (the)",
      "central african republic (the)",
      "chad",
      "chile",
      "china",
      "christmas island",
      "cocos (keeling) islands (the)",
      "colombia",
      "comoros (the)",
      "congo (the democratic republic of the)",
      "congo (the)",
      "cook islands (the)",
      "costa rica",
      "croatia",
      "cuba",
      "curaçao",
      "cyprus",
      "czechia",
      "côte d'ivoire",
      "denmark",
      "djibouti",
      "dominica",
      "dominican republic (the)",
      "ecuador",
      "egypt",
      "el salvador",
      "equatorial guinea",
      "eritrea",
      "estonia",
      "eswatini",
      "ethiopia",
      "falkland islands (the) [malvinas]",
      "faroe islands (the)",
      "fiji",
      "finland",
      "france",
      "french guiana",
      "french polynesia",
      "french southern territories (the)",
      "gabon",
      "gambia (the)",
      "georgia",
      "germany",
      "ghana",
      "gibraltar",
      "greece",
      "greenland",
      "grenada",
      "guadeloupe",
      "guam",
      "guatemala",
      "guernsey",
      "guinea",
      "guinea-bissau",
      "guyana",
      "haiti",
      "heard island and mcdonald islands",
      "holy see (the)",
      "honduras",
      "hong kong",
      "hungary",
      "iceland",
      "india",
      "indonesia",
      "iran (islamic republic of)",
      "iraq",
      "ireland",
      "isle of man",
      "israel",
      "italy",
      "jamaica",
      "japan",
      "jersey",
      "jordan",
      "kazakhstan",
      "kenya",
      "kiribati",
      "korea (the democratic people's republic of)",
      "korea (the republic of)",
      "kuwait",
      "kyrgyzstan",
      "lao people's democratic republic (the)",
      "latvia",
      "lebanon",
      "lesotho",
      "liberia",
      "libya",
      "liechtenstein",
      "lithuania",
      "luxembourg",
      "macao",
      "madagascar",
      "malawi",
      "malaysia",
      "maldives",
      "mali",
      "malta",
      "marshall islands (the)",
      "martinique",
      "mauritania",
      "mauritius",
      "mayotte",
      "mexico",
      "micronesia (federated states of)",
      "moldova (the republic of)",
      "monaco",
      "mongolia",
      "montenegro",
      "montserrat",
      "morocco",
      "mozambique",
      "myanmar",
      "namibia",
      "nauru",
      "nepal",
      "netherlands (the)",
      "new caledonia",
      "new zealand",
      "nicaragua",
      "niger (the)",
      "nigeria",
      "niue",
      "norfolk island",
      "northern mariana islands (the)",
      "norway",
      "oman",
      "pakistan",
      "palau",
      "palestine, state of",
      "panama",
      "papua new guinea",
      "paraguay",
      "peru",
      "philippines (the)",
      "pitcairn",
      "poland",
      "portugal",
      "puerto rico",
      "qatar",
      "republic of north macedonia",
      "romania",
      "russian federation (the)",
      "rwanda",
      "réunion",
      "saint barthélemy",
      "saint helena, ascension and tristan da cunha",
      "saint kitts and nevis",
      "saint lucia",
      "saint martin (french part)",
      "saint pierre and miquelon",
      "saint vincent and the grenadines",
      "samoa",
      "san marino",
      "sao tome and principe",
      "saudi arabia",
      "senegal",
      "serbia",
      "seychelles",
      "sierra leone",
      "singapore",
      "sint maarten (dutch part)",
      "slovakia",
      "slovenia",
      "solomon islands",
      "somalia",
      "south africa",
      "south georgia and the south sandwich islands",
      "south sudan",
      "spain",
      "sri lanka",
      "sudan (the)",
      "suriname",
      "svalbard and jan mayen",
      "sweden",
      "switzerland",
      "syrian arab republic",
      "taiwan (province of china)",
      "tajikistan",
      "tanzania, united republic of",
      "thailand",
      "timor-leste",
      "togo",
      "tokelau",
      "tonga",
      "trinidad and tobago",
      "tunisia",
      "turkey",
      "turkmenistan",
      "turks and caicos islands (the)",
      "tuvalu",
      "uganda",
      "ukraine",
      "united arab emirates (the)",
      "great britain",
      "united states minor outlying islands (the)",
      "united states of america (the)",
      "uruguay",
      "uzbekistan",
      "vanuatu",
      "venezuela (bolivarian republic of)",
      "viet nam",
      "virgin islands (british)",
      "virgin islands (u.s.)",
      "wallis and futuna",
      "western sahara",
      "yemen",
      "zambia"
    ];

    const countryCodes = [
        "AF"
        ,"AL"
        ,"DZ"
        ,"AS"
        ,"AD"
        ,"AO"
        ,"AI"
        ,"AQ"
        ,"AG"
        ,"AR"
        ,"AM"
        ,"AW"
        ,"AU"
        ,"AT"
        ,"AZ"
        ,"BS"
        ,"BH"
        ,"BD"
        ,"BB"
        ,"BY"
        ,"BE"
        ,"BZ"
        ,"BJ"
        ,"BM"
        ,"BT"
        ,"BO"
        ,"BQ"
        ,"BA"
        ,"BW"
        ,"BV"
        ,"BR"
        ,"IO"
        ,"BN"
        ,"BG"
        ,"BF"
        ,"BI"
        ,"CV"
        ,"KH"
        ,"CM"
        ,"CA"
        ,"KY"
        ,"CF"
        ,"TD"
        ,"CL"
        ,"CN"
        ,"CX"
        ,"CC"
        ,"CO"
        ,"KM"
        ,"CD"
        ,"CG"
        ,"CK"
        ,"CR"
        ,"HR"
        ,"CU"
        ,"CW"
        ,"CY"
        ,"CZ"
        ,"CI"
        ,"DK"
        ,"DJ"
        ,"DM"
        ,"DO"
        ,"EC"
        ,"EG"
        ,"SV"
        ,"GQ"
        ,"ER"
        ,"EE"
        ,"SZ"
        ,"ET"
        ,"FK"
        ,"FO"
        ,"FJ"
        ,"FI"
        ,"FR"
        ,"GF"
        ,"PF"
        ,"TF"
        ,"GA"
        ,"GM"
        ,"GE"
        ,"DE"
        ,"GH"
        ,"GI"
        ,"GR"
        ,"GL"
        ,"GD"
        ,"GP"
        ,"GU"
        ,"GT"
        ,"GG"
        ,"GN"
        ,"GW"
        ,"GY"
        ,"HT"
        ,"HM"
        ,"VA"
        ,"HN"
        ,"HK"
        ,"HU"
        ,"IS"
        ,"IN"
        ,"ID"
        ,"IR"
        ,"IQ"
        ,"IE"
        ,"IM"
        ,"IL"
        ,"IT"
        ,"JM"
        ,"JP"
        ,"JE"
        ,"JO"
        ,"KZ"
        ,"KE"
        ,"KI"
        ,"KP"
        ,"KR"
        ,"KW"
        ,"KG"
        ,"LA"
        ,"LV"
        ,"LB"
        ,"LS"
        ,"LR"
        ,"LY"
        ,"LI"
        ,"LT"
        ,"LU"
        ,"MO"
        ,"MG"
        ,"MW"
        ,"MY"
        ,"MV"
        ,"ML"
        ,"MT"
        ,"MH"
        ,"MQ"
        ,"MR"
        ,"MU"
        ,"YT"
        ,"MX"
        ,"FM"
        ,"MD"
        ,"MC"
        ,"MN"
        ,"ME"
        ,"MS"
        ,"MA"
        ,"MZ"
        ,"MM"
        ,"NA"
        ,"NR"
        ,"NP"
        ,"NL"
        ,"NC"
        ,"NZ"
        ,"NI"
        ,"NE"
        ,"NG"
        ,"NU"
        ,"NF"
        ,"MP"
        ,"NO"
        ,"OM"
        ,"PK"
        ,"PW"
        ,"PS"
        ,"PA"
        ,"PG"
        ,"PY"
        ,"PE"
        ,"PH"
        ,"PN"
        ,"PL"
        ,"PT"
        ,"PR"
        ,"QA"
        ,"MK"
        ,"RO"
        ,"RU"
        ,"RW"
        ,"RE"
        ,"BL"
        ,"SH"
        ,"KN"
        ,"LC"
        ,"MF"
        ,"PM"
        ,"VC"
        ,"WS"
        ,"SM"
        ,"ST"
        ,"SA"
        ,"SN"
        ,"RS"
        ,"SC"
        ,"SL"
        ,"SG"
        ,"SX"
        ,"SK"
        ,"SI"
        ,"SB"
        ,"SO"
        ,"ZA"
        ,"GS"
        ,"SS"
        ,"ES"
        ,"LK"
        ,"SD"
        ,"SR"
        ,"SJ"
        ,"SE"
        ,"CH"
        ,"SY"
        ,"TW"
        ,"TJ"
        ,"TZ"
        ,"TH"
        ,"TL"
        ,"TG"
        ,"TK"
        ,"TO"
        ,"TT"
        ,"TN"
        ,"TR"
        ,"TM"
        ,"TC"
        ,"TV"
        ,"UG"
        ,"UA"
        ,"AE"
        ,"GB"
        ,"UM"
        ,"US"
        ,"UY"
        ,"UZ"
        ,"VU"
        ,"VE"
        ,"VN"
        ,"VG"
        ,"VI"
        ,"WF"
        ,"EH"
        ,"YE"
        ,"ZM"
    ];

    const countryMap = new Map();
    for(var i = 0 ; i < countries.length; i++) {
        countryMap.set(countries[i], countryCodes[i]);
    }
    return countryMap;
  }
}

export default new GetAllCountryCodeService();
