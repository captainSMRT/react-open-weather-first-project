import axios from "axios"

class GetAllWeatherService {
    executeGetAllWeatherService(city, countryCode){
        //return axios.get('http://api.openweathermap.org/data/2.5/weather?q='+city+',,'+countryCode+'&APPID=f1d5460d776e0649d8f2a68f1ddea91d')
        //return axios.get('http://api.openweathermap.org/data/2.5/weather?q='+city+',,'+countryCode+'&APPID=4234f59aae45fea9dce50836958d030f')

        return axios.get('http://api.openweathermap.org/data/2.5/weather?q='+city+',,'+countryCode+'&APPID=594107127c13947a142077d9e455186c')
    }

    executeGetWeatherByID(id){
        //return axios.get('http://api.openweathermap.org/data/2.5/weather?id='+id+'&APPID=f1d5460d776e0649d8f2a68f1ddea91d')
        //return axios.get('http://api.openweathermap.org/data/2.5/weather?id='+id+'&APPID=4234f59aae45fea9dce50836958d030f')

        return axios.get('http://api.openweathermap.org/data/2.5/weather?id='+id+'&APPID=594107127c13947a142077d9e455186c')
    }
}

export default new GetAllWeatherService()